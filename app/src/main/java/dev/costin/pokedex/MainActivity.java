package dev.costin.pokedex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;

import dev.costin.pokedex.Common.Common;
import dev.costin.pokedex.Model.Pokemon;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;


    BroadcastReceiver showPokemonType = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().toString().equals(Common.KEY_POKEMON_TYPE)){

                //Remplacer le Fragment
                Fragment pokemonType = PokemonType.getInstance();
                String type = intent.getStringExtra("type");
                Bundle bundle = new Bundle();
                bundle.putString("type", type);
                pokemonType.setArguments(bundle);


                getSupportFragmentManager().popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.list_pokemon_fragment, pokemonType);
                fragmentTransaction.addToBackStack("type");
                fragmentTransaction.commit();

                //Set un nom de Type pour la Toolbar
                toolbar.setTitle("POKEMON TYPE : " + type.toUpperCase());
            }
        }
    };

    BroadcastReceiver showDetails = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().toString().equals(Common.KEY_ENABLE_HOME)){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Activation du boutton qui permet de retourner en arrière sur la Toolbar
                getSupportActionBar().setDisplayShowHomeEnabled(true); //Idem

                //Remplacer le Fragment
                Fragment detailsFragment = PokemonDetails.getInstance();
                String num = intent.getStringExtra("num");
                Bundle bundle = new Bundle();
                bundle.putString("num", num);
                detailsFragment.setArguments(bundle);

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.list_pokemon_fragment, detailsFragment);
                fragmentTransaction.addToBackStack("details");
                fragmentTransaction.commit();

                //Set un nom de Pokémon pour la Toolbar
                Pokemon pokemon = Common.findPokemonByNum(num);
                toolbar.setTitle(pokemon.getName());
            }
        }
    };


    BroadcastReceiver showEvolutions = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().toString().equals(Common.KEY_NUM_EVOLUTION)){

                //Remplacer le Fragment
                Fragment detailsFragment = PokemonDetails.getInstance();
                Bundle bundle = new Bundle();
                String num = intent.getStringExtra("num");
                bundle.putString("num", num);
                detailsFragment.setArguments(bundle);

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.remove(detailsFragment); // Supprimer le Fragment actuel
                fragmentTransaction.replace(R.id.list_pokemon_fragment, detailsFragment);
                fragmentTransaction.addToBackStack("details");
                fragmentTransaction.commit();

                //Set un nom de Pokémon pour la Toolbar
                Pokemon pokemon = Common.findPokemonByNum(num);
                toolbar.setTitle(pokemon.getName());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("LISTE DE TOUT LES POKEMONS");
        setSupportActionBar(toolbar);

        //Enregistrer le BroadCast
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(showDetails, new IntentFilter(Common.KEY_ENABLE_HOME));

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(showEvolutions, new IntentFilter(Common.KEY_NUM_EVOLUTION));

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(showPokemonType, new IntentFilter(Common.KEY_POKEMON_TYPE));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                toolbar.setTitle("LISTE DE POKEMON");

                //Permet de clear tout les framgents details et de les supprimer aussi de la liste des fragments
                getSupportFragmentManager().popBackStack("details", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().popBackStack("type", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                //Remplacer les Fragments
                Fragment pokemonList = PokemonList.getInstance();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.remove(pokemonList);
                fragmentTransaction.replace(R.id.list_pokemon_fragment, pokemonList);
                fragmentTransaction.commit();

                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                break;
                default:
                    break;
        }
        return true;
    }
}